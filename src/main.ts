import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import Index from '@/pages/Index.vue'
import Book from '@/pages/Book.vue'
import './assets/style.css'
axios.defaults.baseURL = 'https://openlibrary.org/'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    results: [],
    actualPage: 1
  },
  mutations: {
    setResults (state, payload) {
      state.results = payload
    },
    setactualPage (state, payload) {
      state.actualPage = payload
    }
  }
})

const routes = [
  { path: '/', component: Index },
  { path: '/book/:key', name: 'book', component: Book }
]

const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})

Vue.use(VueRouter)

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
