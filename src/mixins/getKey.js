/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export default function (object) {
  let key = ''
  if (object.isbn) {
    key = 'isbn'
  } else if (object.oclc) {
    key = 'oclc'
  } else if (object.lccn) {
    key = 'lccn'
  } else if (object.olid) {
    key = 'olid'
  }

  return key
}
